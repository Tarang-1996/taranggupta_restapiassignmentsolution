package com.employee.management.service;

import org.springframework.stereotype.Service;

import com.employee.management.exception.ResourceAlreadyExistException;
import com.employee.management.model.Role;
/**
 * 
 * @author Tarang Gupta
 *
 */
@Service
public interface RoleService {

	/**
	 * to save the role details
	 * 
	 * @return saved role @throws
	 * @throws ResourceAlreadyExistException
	 */
	Role saveRole(Role role) throws ResourceAlreadyExistException;

}