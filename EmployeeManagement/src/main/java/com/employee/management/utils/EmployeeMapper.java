package com.employee.management.utils;

import java.util.List;
import java.util.stream.Collectors;

import com.employee.management.dto.EmployeeDto;
import com.employee.management.model.Employee;
/**
 * to map the Employee to EmployeeDto and vice versa
 * 
 * @author Tarang Gupta
 *
 */
public final class EmployeeMapper {
	
	
	/**
	 * to map the Employee object to EmployeeDto
	 * 
	 * @param employee
	 * @return EmployeeDto
	 */
	public static EmployeeDto modelToDto(Employee employee) {
		
		return EmployeeDto.builder()
				.id(employee.getId())
				.firstName(employee.getFirstName())
				.lastName(employee.getLastName())
				.email(employee.getEmail())
				.build();
	}
	
	/**
	 * to map EmployeeDto to Employee
	 * @param dto
	 * @return Employee
	 */
	
	public static Employee dtoToModel(EmployeeDto dto) {
		
		return Employee.builder()
				.id(dto.getId())
				.firstName(dto.getFirstName())
				.lastName(dto.getLastName())
				.email(dto.getEmail())
				.build();
	}
	
	/**
	 * to map List<EmployeeDto> to List<Employee>
	 * 
	 * @param employees- list of EmployeeDto
	 * @return list of Employee
	 */
	
	public static List<Employee> dtoToModel(List<EmployeeDto> employees) {
		
		return employees.stream().map(dto->Employee.builder()
				.id(dto.getId())
				.firstName(dto.getFirstName())
				.lastName(dto.getLastName())
				.email(dto.getEmail())
				.build())
				.collect(Collectors.toList());
	}

}
