package com.employee.management;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

import com.employee.management.model.Role;
import com.employee.management.model.User;
import com.employee.management.service.UserServiceImpl;
/**
 * to create a user for first time login
 * 
 * @author Tarang Gupta
 *
 */
@Component
public class DataLoader implements ApplicationRunner {

    private UserServiceImpl userService;

    @Autowired
    public DataLoader(UserServiceImpl userService) {
        this.userService = userService;
       
    }

    public void run(ApplicationArguments args) {
    	   	
    	Role role = Role.builder().name("ADMIN").build();
    	User user= User.builder().username("Admin").password("Admin@2022").build();
    	user.addRole(role);
    	userService.saveUser(user);
    }
}
