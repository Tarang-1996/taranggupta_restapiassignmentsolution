package com.employee.management.model;

import java.util.ArrayList;
import java.util.List;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

import com.employee.management.utils.ValidPassword;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
/**
 * to define user model
 * 
 * @author Tarang Gupta
 *
 */

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder()
public class User {

	
	private int id;

	@NotEmpty(message = "Username can not be null or blank")
	@Size(min = 2, max = 50)
	private String username;

	@NotEmpty(message = "Password can not be empty")
	@ValidPassword
	@Size(min = 8, max = 30)
	private String password;
	
	private List<Role> roles;
	
	public void addRole(Role role) {
		
		if(this.roles==null) {
			this.roles= new ArrayList<>();
			
		}
		roles.add(role);
			
	}

}