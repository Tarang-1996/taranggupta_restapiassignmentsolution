package com.employee.management.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.employee.management.model.User;
import com.employee.management.service.UserServiceImpl;

import lombok.extern.slf4j.Slf4j;

@RestController
@RequestMapping("/user")
@Slf4j
@Validated
public class UserController {

	@Autowired
	UserServiceImpl userService;
	
	
	@PostMapping("/")
	public ResponseEntity<String> save(@Valid @RequestBody User user) {

		log.info("Request recieved to save the user");
		String savedUser = userService.saveUser(user);
		return new ResponseEntity<String>(savedUser, HttpStatus.CREATED);
	}

}
