package com.employee.management.exception;
/**
 * to define the exception ResourceAlreadyExistException
 * 
 * @author Tarang Gupta
 *
 */
public class ResourceAlreadyExistException extends RuntimeException  {

	
	private static final long serialVersionUID = 680180958395007644L;
	
	ResourceAlreadyExistException() {

	}

	public ResourceAlreadyExistException(String email) {
		super("Resource already exist: "+email);
	}

}
