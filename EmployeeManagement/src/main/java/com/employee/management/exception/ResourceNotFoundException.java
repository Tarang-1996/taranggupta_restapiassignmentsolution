package com.employee.management.exception;


/**
 * to define exception for ResourceNotFoundException
 * 
 * @author Tarang Gupta
 *
 */
public class ResourceNotFoundException extends RuntimeException  {

	
	private static final long serialVersionUID = 1L;

	ResourceNotFoundException() {

	}

	public ResourceNotFoundException(int id) {
		super("Resource not found with id: "+id);
	}
	
	public ResourceNotFoundException(String firstName) {
		super("Resource not found with Name: "+firstName);
	}

}
