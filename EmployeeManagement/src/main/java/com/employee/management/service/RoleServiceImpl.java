/**
 * 
 */
package com.employee.management.service;

import static com.employee.management.utils.RoleMapper.dtoToModel;
import static com.employee.management.utils.RoleMapper.modelToDto;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.employee.management.dto.RoleDto;
import com.employee.management.exception.ResourceAlreadyExistException;
import com.employee.management.model.Role;
import com.employee.management.repository.RoleRepository;

import lombok.extern.slf4j.Slf4j;

/**
 * to peform business operation for role
 * 
 * @author Tarang Gupta
 *
 */
@Service
@Slf4j
public class RoleServiceImpl implements RoleService {

	@Autowired
	RoleRepository roleRepo;

	/**
	 * to save the role details
	 * 
	 * @return saved role @throws
	 * @throws ResourceAlreadyExistException
	 */
	@Override
	public Role saveRole(Role role) throws ResourceAlreadyExistException {

		log.info("Saving the role");

		Optional<RoleDto> existingRole = roleRepo.findByName(role.getName());

		if (existingRole.isPresent()) {

			log.error("Error while saving the role, Role already exist " + role.getName());
			throw new ResourceAlreadyExistException(role.getName());
		}

		Role savedRole = dtoToModel(roleRepo.save(modelToDto(role)));
		roleRepo.flush();

		log.info("Saved role " + savedRole.getName());
		return savedRole;
	}

}
