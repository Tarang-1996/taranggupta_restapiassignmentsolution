package com.employee.management.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;

import com.employee.management.dto.EmployeeDto;
import com.employee.management.exception.ResourceAlreadyExistException;
import com.employee.management.exception.ResourceNotFoundException;
import com.employee.management.model.Employee;
import com.employee.management.repository.EmployeeRepository;
import com.employee.management.utils.EmployeeMapper;

import static com.employee.management.utils.EmployeeMapper.dtoToModel;
import static com.employee.management.utils.EmployeeMapper.modelToDto;

import lombok.extern.slf4j.Slf4j;

/**
 * to implement employee related business logic
 * 
 * @author Tarang Gupta
 *
 */
@Service
@Slf4j
public class EmployeeServiceImpl implements EmployeeService {

	@Autowired
	EmployeeRepository employeeRepo;

	/**
	 * to list all the employees stored in the database.
	 * 
	 * @return list of all employees
	 *
	 */
	@Override
	public List<Employee> findAll() {
		
		log.info("Fetching list of employees");
		return EmployeeMapper.dtoToModel(employeeRepo.findAll());
	}

	/**
	 * to search the employee by id
	 * 
	 * @return employee
	 * @throws ResourceNotFoundException
	 *
	 */

	@Override
	public Employee findById(int id) throws ResourceNotFoundException {
		
		log.info("Searching employee by id");
		return employeeRepo.findById(id)
				.map(dto->EmployeeMapper.dtoToModel(dto))
				.orElseThrow(() -> new ResourceNotFoundException(id));
	}

	/**
	 * to save the employee
	 * 
	 * @return saved employee detail
	 * @throws ResourceAlreadyExistException
	 *
	 */
	@Override
	public Employee save(Employee employee) throws ResourceAlreadyExistException {
		
		log.info("Saving the employee");

		String email = employee.getEmail();

		EmployeeDto existingEmployee = employeeRepo.findByEmail(email);

		if (employee.getId() == 0 && existingEmployee != null) {
			log.error("Employee already Exist with email "+email);
			throw new ResourceAlreadyExistException(email);
		}

		Employee emp = dtoToModel(employeeRepo.save(modelToDto(employee)));
		employeeRepo.flush();
		log.info("Employee is saved "+emp);
		return emp;
	}

	/**
	 * to update the employee- - before updating check if employee exist
	 * 
	 * @return employee
	 * @throws ResourceNotFoundException
	 *
	 */
	@Override
	public Employee update(Employee employee) throws ResourceNotFoundException {
		
		log.info("updating the employee");
		int id = employee.getId();
		Employee existingEmployee = this.findById(id);

		existingEmployee.setFirstName(employee.getFirstName());
		existingEmployee.setLastName(employee.getLastName());
		existingEmployee.setEmail(employee.getEmail());

		Employee emp = dtoToModel(employeeRepo.save(modelToDto(employee)));
		employeeRepo.flush();
		
		log.info("Employee is updated"+emp );
		return emp;

	}

	/**
	 * 
	 * to delete employee by id
	 * 
	 * @return String it returns the message
	 * @throws ResourceNotFoundException
	 * 
	 */
	@Override
	public String delete(int id) throws ResourceNotFoundException {
		
		log.info("Going to delete the Employee");
		 
		this.findById(id);
		employeeRepo.deleteById(id);
		
		log.info("Employee is deleted "+id);

		return "Deleted Employee id - " + id;

	}

	/**
	 * to fetch the employee by firstName
	 * 
	 * @return List<Employee> list of employee
	 * @throws ResourceNotFoundException
	 */
	@Override
	public List<Employee> findByFirstName(String firstName) throws ResourceNotFoundException {
		
		log.info("Fetching the list of employees by first name");
		List<EmployeeDto> employees = employeeRepo.findByFirstName(firstName);

		if (employees == null) {
			log.error("Error while Fetching the list of employees by first name, No employee found");
			throw new ResourceNotFoundException(firstName);
		}
		
		log.info("Employees are:"+ employees);
		return dtoToModel(employees);
	}

	/**
	 * to list the employee sorted by firstName
	 * 
	 * @return List<Employee> list of employee
	 * 
	 */
	@Override
	public List<Employee> findAllSortedByFirstName(Direction direction) {
		
		log.info("Fetching the employees sorted by first name");
		return dtoToModel(employeeRepo.findAll(Sort.by(direction, "firstName")));

	}

}
