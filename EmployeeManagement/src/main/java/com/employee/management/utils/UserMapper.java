package com.employee.management.utils;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import com.employee.management.dto.UserDto;
import com.employee.management.model.User;
/**
 * to map the User to UserDto and vice-versa
 * @author Tarang Gupta
 *
 */
public final class UserMapper {
	
	
	/**
	 * to map the User to UserDto
	 * 
	 * @param user
	 * @return UserDto
	 */
	
	public static UserDto modelToDtoMapper(User user) {

		if (user.getRoles() != null) {
			return UserDto.builder()
					.username(user.getUsername())
					.password(new BCryptPasswordEncoder().encode(user.getPassword()))
					.roles(RoleMapper.modelToDto(user.getRoles()))
					.build();
		}
		return UserDto.builder()
				.username(user.getUsername())
				.password(new BCryptPasswordEncoder().encode(user.getPassword()))
				.build();

	}

}
