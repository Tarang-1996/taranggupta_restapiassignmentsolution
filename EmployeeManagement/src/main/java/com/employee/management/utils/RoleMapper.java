package com.employee.management.utils;

import java.util.List;
import java.util.stream.Collectors;

import com.employee.management.dto.RoleDto;
import com.employee.management.model.Role;
/**
 * to map the Role to RoleDto and vice versa
 * 
 * @author Tarang Gupta
 *
 */
public final class RoleMapper {

	
	/**
	 * to convert the Role to RoleDto
	 * 
	 * @param role
	 * @return RoleDto
	 */
	public static RoleDto modelToDto(Role role) {

		return RoleDto.builder()
				.name("ROLE_" + role.getName().toUpperCase())
				.build();
	}
	
	/**
	 * to map the List<Role> to List<RoleDto>
	 * 
	 * @param roles- list of Role
	 * @return list of RoleDto
	 */
	
	public static List<RoleDto> modelToDto(List<Role> roles) {

		return roles.stream()
				.map(role->RoleDto.builder()
				.name("ROLE_" + role.getName().toUpperCase())
				.build())
				.collect(Collectors.toList());
	}

	
	/**
	 * to convert the RoleDto to Role
	 * 
	 * @param dto
	 * @return Role
	 */
	public static Role dtoToModel(RoleDto dto) {

		return Role.builder()
				.id(dto.getId())
				.name(dto.getName().replaceFirst("ROLE_", ""))
				.build();
	}

}
