/**
 * 
 */
package com.employee.management.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.employee.management.model.Role;
import com.employee.management.service.RoleService;

import lombok.extern.slf4j.Slf4j;

/**
 * @author Tarang Gupta
 *
 */
@RestController
@RequestMapping("/role")
@Slf4j
@Validated
public class RoleController {
	
	@Autowired
	RoleService roleService;
	
	@PostMapping("/")
	public ResponseEntity<Role> saveRole(@Valid @RequestBody Role role){
		
		log.info("Request recieved to save the Role");
		Role savedRole = roleService.saveRole(role);
		
		return new ResponseEntity<Role>(savedRole,HttpStatus.CREATED);
	}

}
