/**
 * 
 */
package com.employee.management.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.employee.management.model.Employee;
import com.employee.management.service.EmployeeService;

import lombok.extern.slf4j.Slf4j;

/**
 * @author Tarang Gupta
 *
 */
@RestController
@RequestMapping("/api")
@Slf4j
@Validated
public class EmployeeController {

	@Autowired
	EmployeeService employeeService;

	@PostMapping("/employee")
	public ResponseEntity<Employee> saveEmployee(@Valid @RequestBody Employee employee) {

		log.info("Request received for saving the employee");
		Employee savedEmployee = employeeService.save(employee);
		return new ResponseEntity<Employee>(savedEmployee, HttpStatus.CREATED);
	}

	@GetMapping("/employee/list")
	public ResponseEntity<List<Employee>> listAll() {

		log.info("Request received for listing the employee");
		List<Employee> employees = employeeService.findAll();
		return new ResponseEntity<List<Employee>>(employees, HttpStatus.OK);
	}

	@GetMapping("/employee/{id}")
	public ResponseEntity<Employee> getEmployeeById(@PathVariable("id") int id) {

		log.info("Request received for get  the employee by id");
		Employee employee = employeeService.findById(id);
		return new ResponseEntity<Employee>(employee, HttpStatus.OK);
	}

	@PutMapping("/employee")
	public ResponseEntity<Employee> updateEmployee(@Valid @RequestBody Employee employee) {

		log.info("Request received for updating the employee");
		Employee UpdatedEmployee = employeeService.update(employee);
		return new ResponseEntity<Employee>(UpdatedEmployee, HttpStatus.OK);
	}

	@DeleteMapping("/employee/{id}")
	public ResponseEntity<String> deleteEmployeeById(@PathVariable("id") int id) {

		log.info("Request received for deleting the employee");
		String message = employeeService.delete(id);
		return new ResponseEntity<String>(message, HttpStatus.OK);
	}

	@GetMapping("/employee/search/{firstName}")
	public ResponseEntity<List<Employee>> getEmployeeByFirstName(@PathVariable("firstName") String firstName) {

		log.info("Request received for get the employee by first Name");
		List<Employee> employees = employeeService.findByFirstName(firstName);
		return new ResponseEntity<List<Employee>>(employees, HttpStatus.OK);
	}

	@GetMapping("/employee/sort")
	public ResponseEntity<List<Employee>> listSortedByFirstName(@RequestParam(name="order",required=true) Direction direction) {

		log.info("Request received for listing the employee sorted by first Name");
		List<Employee> employees = employeeService.findAllSortedByFirstName(direction);
		return new ResponseEntity<List<Employee>>(employees, HttpStatus.OK);
	}

}
