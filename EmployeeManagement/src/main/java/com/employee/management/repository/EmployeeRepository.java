/**
 * 
 */
package com.employee.management.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.employee.management.dto.EmployeeDto;

/**
 * to Manage employee table
 * 
 * @author Tarang Gupta
 *
 */
@Repository
public interface EmployeeRepository extends JpaRepository<EmployeeDto,Integer>{
	
	
	public EmployeeDto findByEmail(String email);
	
	public List<EmployeeDto> findByFirstName(String firstName);
	
	

}
