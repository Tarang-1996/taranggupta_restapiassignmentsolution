/**
 * 
 */
package com.employee.management.utils;

import java.util.Arrays;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.passay.AlphabeticalSequenceRule;
import org.passay.DigitCharacterRule;
import org.passay.LengthRule;
import org.passay.NumericalSequenceRule;
import org.passay.PasswordData;
import org.passay.PasswordValidator;
import org.passay.QwertySequenceRule;
import org.passay.RuleResult;
import org.passay.SpecialCharacterRule;
import org.passay.UppercaseCharacterRule;
import org.passay.WhitespaceRule;

import com.google.common.base.Joiner;

/**
 * to define the custom password constraints
 * 
 * @author Tarang Gupta
 *
 */
public class PasswordConstraintValidator implements ConstraintValidator<ValidPassword, String> {

	@Override
	public void initialize(ValidPassword arg0) {
	}

	/**
	 * to create a password validator to validate below rules- 
	 * 1- Minimum length of password should be 8 and maximum length should be 30 
	 * 2- Atleast one Upper Case Chararcter 
	 * 3- Atleast one Digit chararcter 
	 * 4- Atleast one special character - "!\"#$%&'()*+,-./:;<=>?@[\\]^_`{|}~" 
	 * 5- password should not contains more than 2 digit numerical keyboard sequence 
	 * 6- password should not contain more than 2 digit alphabetical sqeuence 
	 * 7- password should not contain more than 2 digit QWERTY keyboard sequence 
	 * 8- password should not contain whitespace character e.g. TAB,LF,VT,FF,CR,Space
	 * 
	 * @return true/fasle- as per validation result
	 * 
	 */

	@Override
	public boolean isValid(String password, ConstraintValidatorContext context) {
		PasswordValidator validator = new PasswordValidator(Arrays.asList(new LengthRule(8, 30),
				new UppercaseCharacterRule(1), new DigitCharacterRule(1), new SpecialCharacterRule(1),
				new NumericalSequenceRule(3, false), new AlphabeticalSequenceRule(3, false),
				new QwertySequenceRule(3, false), new WhitespaceRule()));

		RuleResult result = validator.validate(new PasswordData(password));
		if (result.isValid()) {
			return true;
		}

		// Disables the default contraint voilation messgae
		context.disableDefaultConstraintViolation();

		// to create the custome violation message
		context.buildConstraintViolationWithTemplate(Joiner.on(",").join(validator.getMessages(result)))
				.addConstraintViolation();
		
		return false;
	}
}
