/**
 * 
 */
package com.employee.management.service;

import static com.employee.management.utils.UserMapper.modelToDtoMapper;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import com.employee.management.dto.UserDto;
import com.employee.management.exception.ResourceAlreadyExistException;
import com.employee.management.exception.ResourceNotFoundException;
import com.employee.management.model.User;
import com.employee.management.repository.UserRepository;
import com.employee.management.security.ApplicationUserDetails;

import lombok.extern.slf4j.Slf4j;

/**
 * To perform the business logic for user
 * 
 * @author Tarang Gupta
 *
 */
@Service
@Slf4j
@Primary
public class UserServiceImpl implements UserDetailsService {

	@Autowired
	UserRepository userRepo;

	/**
	 * to save the user details
	 * 
	 * @return saved user @throws
	 * @throws ResourceAlreadyExistException
	 */
	public String saveUser(User user) throws ResourceAlreadyExistException {

		log.info("Saving the user");

		Optional<UserDto> existingUser = userRepo.findByUsername(user.getUsername());

		if (existingUser.isPresent()) {
			log.error("Error while saving the user, User already exist " + user.getUsername());
			throw new ResourceAlreadyExistException(existingUser.get().getUsername());
		}

		System.out.println("encoded password is:" + new BCryptPasswordEncoder().encode(user.getPassword()));

		UserDto saveUser = userRepo.save(modelToDtoMapper(user));
		userRepo.flush();

		log.info("Saved User " + saveUser.getUsername());

		return "User saved successfully: " + saveUser.getUsername();
	}

	/**
	 * to find the user by userName
	 * 
	 * @return user details
	 * @throws ResourceNotFoundException
	 */
	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		UserDto user = userRepo.findByUsername(username).orElseThrow(() -> new UsernameNotFoundException(username));

		return new ApplicationUserDetails(user);
	}

}
