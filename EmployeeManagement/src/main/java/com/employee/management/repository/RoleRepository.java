/**
 * 
 */
package com.employee.management.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.employee.management.dto.RoleDto;

/**
 * to manage role table
 * 
 * 
 * @author Tarang Gupta
 *
 */
@Repository
public interface RoleRepository extends JpaRepository<RoleDto, Integer> {
	
	Optional<RoleDto> findByName(String name);

}
