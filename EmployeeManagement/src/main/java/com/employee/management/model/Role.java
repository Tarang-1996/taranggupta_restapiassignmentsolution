package com.employee.management.model;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
/**
 * to define role model
 * 
 * @author Tarang Gupta
 *
 */

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder()
public class Role {

	
	private int id;

	@NotEmpty(message = "Role can not ne blank or null")
	@Size(min = 2, max = 50)
	private String name;
	
}
