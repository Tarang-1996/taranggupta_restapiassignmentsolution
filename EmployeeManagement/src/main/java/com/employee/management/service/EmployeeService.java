package com.employee.management.service;

import java.util.List;

import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;

import com.employee.management.exception.ResourceAlreadyExistException;
import com.employee.management.exception.ResourceNotFoundException;
import com.employee.management.model.Employee;

/**
 * 
 * @author Tarang Gupta
 *
 */
@Service
public interface EmployeeService {

	/**
	 * to list all the employees stored in the database.
	 * 
	 * @return list of all employees
	 *
	 */
	List<Employee> findAll();

	/**
	 * to search the employee by id
	 * 
	 * @return employee
	 * @throws ResourceNotFoundException
	 *
	 */

	Employee findById(int id) throws ResourceNotFoundException;

	/**
	 * to save the employee
	 * 
	 * @return saved employee detail
	 * @throws ResourceAlreadyExistException
	 *
	 */
	Employee save(Employee employee) throws ResourceAlreadyExistException;

	/**
	 * to update the employee- - before updating check if employee exist
	 * 
	 * @return employee
	 * @throws ResourceNotFoundException
	 *
	 */
	Employee update(Employee employee) throws ResourceNotFoundException;

	/**
	 * 
	 * to delete employee by id
	 * 
	 * @return String it returns the message
	 * @throws ResourceNotFoundException
	 * 
	 */
	String delete(int id) throws ResourceNotFoundException;

	/**
	 * to fetch the employee by firstName
	 * 
	 * @return List<Employee> list of employee
	 * @throws ResourceNotFoundException
	 */
	List<Employee> findByFirstName(String firstName) throws ResourceNotFoundException;

	/**
	 * to list the employee sorted by firstName
	 * 
	 * @return List<Employee> list of employee
	 * 
	 */
	List<Employee> findAllSortedByFirstName(Direction direction);

}