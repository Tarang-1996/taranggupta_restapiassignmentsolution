package com.employee.management.model;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * to create the employee Model
 * 
 * @author Tarang Gupta
 *
 */

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder()
public class Employee {
	
	private int id;

	@NotEmpty(message = "First Name can not be null or blank")
	@Size(min = 1, max = 50)
	private String firstName;

	@NotEmpty(message = "Last Name can not be null or  blank")
	@Size(min = 1, max = 50)
	private String lastName;

	@Email(message = "Email should be valid")
	private String email;

}
