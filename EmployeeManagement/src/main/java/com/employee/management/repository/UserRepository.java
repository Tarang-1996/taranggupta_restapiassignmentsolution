/**
 * 
 */
package com.employee.management.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.employee.management.dto.UserDto;

/**
 * to manage user table
 * 
 * 
 * @author Tarang Gupta
 *
 */
@Repository
public interface UserRepository  extends JpaRepository<UserDto, Integer>{

	Optional<UserDto> findByUsername(String username);
}
